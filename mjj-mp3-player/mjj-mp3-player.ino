#include <MP3Trigger.h>

MP3Trigger mp3;
const int BLINK = 13;
const int DEBOUNCE = 750;

typedef enum { NONE, MUSIC, STORIES } State;

volatile State  state         = NONE;
volatile int    MTRACK        = 1;
volatile int    STRACK        = 128;
volatile int    then          = 0;

// MUSIC BUTTON
void yellowButton () {
  int now = millis();
  if (now - then > DEBOUNCE) {
    switch (state) {
      
      case NONE:
        state = MUSIC;
        mp3.trigger (1);   
        break;
        
      case MUSIC:
        MTRACK += 1;
        mp3.trigger (MTRACK);
        break;
  
      case STORIES:
        state = MUSIC;
        mp3.trigger (MTRACK);
        break;
    } 
  }
  then = now;
}

// STORY BUTTON
void greenButton () {
  int now = millis();
  if (now - then > DEBOUNCE) {
    switch (state) {
      
      case NONE:
        state = STORIES;
        mp3.trigger (128); 
        break;
        
      case MUSIC:
        state = STORIES;
        mp3.trigger (STRACK); 
        break;
  
      case STORIES:
        STRACK += 1;
        mp3.trigger (STRACK);
        break;
    }
  }
  then = now;
}

void setup() {
  // Start serial communication with the trigger (over Serial)
  mp3.setup(&Serial);
  Serial.begin( MP3Trigger::serialRate() );
  
  pinMode (BLINK, OUTPUT);
  
  // Wait for the MP3 Trigger
  digitalWrite (BLINK, HIGH);
  delay(2000);
  digitalWrite (BLINK, LOW);
  
  // MUSIC BUTTON
  attachInterrupt (digitalPinToInterrupt (3), yellowButton, LOW);
  // STORY BUTTON
  attachInterrupt (digitalPinToInterrupt (2), greenButton, LOW);
}

void loop() {
  HardwareSerial *s = &Serial;
  
  if( s->available() )
  {
    int data = s->read();
    switch(char(data))
    {
      // If a track ends, play the next one.
      case 'X': 
        digitalWrite (BLINK, HIGH);
        delay (750);
        digitalWrite (BLINK, LOW);
        switch (state) {
          case NONE:
            break;

          case MUSIC:
            MTRACK += 1;
            mp3.trigger(MTRACK);
            break;

          case STORIES:
            STRACK += 1;
            mp3.trigger(STRACK);
            break;
        }
        break;

      // If we get an error, go back to the beginning.
      case 'E':
        delay(750);
        switch (state) {
          case NONE:
            break;

          case MUSIC:
            MTRACK = 1;
            mp3.trigger(MTRACK);
            break;

          case STORIES:
            STRACK = 128;
            mp3.trigger(STRACK);
            break;
        }
        break;
        
      default:
        break;
    }
  }

}
